package com.testml;


import org.junit.Assert;
import org.junit.Test;

public class WordProcesserTest {

	@Test
	public void testEncodeWhenStringEmpty() throws Exception {
		String original = "";
		String expected = "";
		String actual = WordProcesser.encode(1, original);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testEncodeWhenOriginalStringNull() throws Exception {
		String original = null;
		Assert.assertNull(WordProcesser.encode(1, original));
	}

	@Test
	public void testEncodePositiveOffset() throws Exception {
		String original = "bacabcdefg";
		String expected = "cbdbcdefgh";
		String actual = WordProcesser.encode(1, original);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testEncodePositiveOffsetAndSpecialChar() throws Exception {
		String original = "b0aca_bcd#efg$";
		String expected = "c0bdb_cde#fgh$";
		String actual = WordProcesser.encode(1, original);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testEncodeNegativeOffset() throws Exception {
		String original = "bacabcdefg";
		String expected = "azbzabcdef";
		String actual = WordProcesser.encode(- 1, original);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testEncodeNigativeOffsetAndSpecialChar() throws Exception {
		String original = "b0aca_bcd#efg$";
		String expected = "a0zbz_abc#def$";
		String actual = WordProcesser.encode(-1, original);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testEncodeffsetAndSpecialCharOnly() throws Exception {
		String original = "!@#$&$^$^$^()";
		String expected = "!@#$&$^$^$^()";
		String actual = WordProcesser.encode(-1, original);
		Assert.assertEquals(expected, actual);
	}
}
