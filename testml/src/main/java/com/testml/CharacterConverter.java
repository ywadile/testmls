package com.testml;

/**
 * Functional Interface use to convert one char to another
 */
@FunctionalInterface
public interface CharacterConverter {
	/**
	 * Method take input as character which want to convert next/previous charater depend on offset value.
	 * Offset is 1 then all 'a' letters are replaced with 'b', 'b' with 'c', etc.
	 * Offset is -1 then all 'a' letters are replaced with 'z', 'b' with 'a', etc.
	 * @param currentChar - it contain character, digit, special character.
	 * @param offset 1 or -1
	 * @return
	 */
		char applyAsChar(char currentChar, int offset);
}
