package com.testml;

public class WordProcesser {

	/**
	 * This is implementation and rule use as functional interface.
	 * if Character is other than alphabhate return same character
	 * If offset 1 and last character (z or Z) return a or A
	 * If offset -1 and last character (a or a) return z or Z
	 * otherwise return char + offset.
	 */
	public static CharacterConverter cc = (char currentChar, int runOffset) -> {
		if (!Character.isAlphabetic(currentChar)) {
			return currentChar;
		} else if (runOffset == 1 && (currentChar == 'z' || currentChar == 'Z')) {
			return Character.isUpperCase(currentChar) ? 'A': 'a';
		} else if (runOffset == -1 && (currentChar == 'a' || currentChar == 'A')) {
			return Character.isUpperCase(currentChar) ? 'Z' : 'z';
		}
		return (char) (currentChar + runOffset);
	};

	/**
	 * Method actual use to process string and convert depend on rule set.
	 * Offset is 1 then all 'a' letters are replaced with 'b', 'b' with 'c', etc.
	 * Offset is -1 then all 'a' letters are replaced with 'z', 'b' with 'a', etc.
	 * @param offset 1 or -1
	 * @param original any String
	 * @return converted string
	 */
	public static String encode(int offset, String original) {
		if (original == null)
			return original;

		String characterStream = original.chars()
			.mapToObj(value -> cc.applyAsChar((char) value, offset))
			.collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
			.toString();

		return characterStream;
	}

}
