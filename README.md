*** Java Version *** 1.8.45

*** Build tool ***   Maven.

*** Dependancy *** 

```
<dependency>
	<groupId>junit</groupId>
	<artifactId>junit</artifactId>
	<scope>test</scope>
	<version>4.12</version>
</dependency>
		
```
*** What's in project repository *** 

src/main/java/com/testml/WordProcesser.java - Main class where encoding logic is return.

src/test/java/com/testml/WordProcesserTest.java - Junit class use to test Functionality of WordProcesser.encode().